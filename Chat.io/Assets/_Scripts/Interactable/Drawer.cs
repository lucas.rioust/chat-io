﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drawer : MonoBehaviour
{
    [SerializeField]
    private TrailRenderer trailrendererPrefab;

    private TrailRenderer currentTrailRenderer;

    private Camera camera;

    [SerializeField]
    private MeshRenderer Plane;

    private Texture2D drawingTexture;

    [SerializeField]
    private Color color = Color.red;

    void Start()
    {
        camera = GetComponent<Camera>();
        drawingTexture = new Texture2D(256, 256);
        
        Plane.material.SetTexture("_MainTex", drawingTexture);
    }

    void Update()
    {
        if(Input.GetMouseButton(0))
        {
            Vector2 mousePosition = new Vector3(Input.mousePosition.x,Input.mousePosition.y);
            var pos = camera.ScreenToWorldPoint(mousePosition);
            pos.z += 15.8f;
            RaycastHit hit;
            if(Physics.Raycast(transform.position,pos - transform.position,out hit))
            {
                GetPixelsToDraw(new Vector2((int)(hit.textureCoord.x * drawingTexture.width), (int)(hit.textureCoord.y * drawingTexture.height)), 200, new Vector2(drawingTexture.width,drawingTexture.height));
                foreach(var p in pixelsToDraw)
                    drawingTexture.SetPixel((int)p.x,(int)p.y, color);
                drawingTexture.Apply();
            }
        }
    }

    List<Vector2> pixelsToDraw = new List<Vector2>(1000);
    List<Vector2> pixelsNotToDraw = new List<Vector2>(1000);

    private void GetPixelsToDraw(Vector2 startPosition, int radius, Vector2 textSize)
    {
        pixelsToDraw.Clear();
        pixelsToDraw.Add(startPosition);
        pixelsNotToDraw.Clear();
        Vector2 lastAddedPixel = startPosition;
        Vector2 direction = new Vector2(-1, 0);
        Debug.Log(textSize);
        for(int i = 0; i < radius;i++)
        {
            if (IsPixelInTexture(lastAddedPixel + direction, textSize))
            {
                pixelsToDraw.Add(lastAddedPixel + direction);
            }
            else pixelsNotToDraw.Add(lastAddedPixel + direction);
                
            lastAddedPixel = lastAddedPixel + direction;
            var targetDir = Rotate90(direction);
            if(!pixelsToDraw.Contains(lastAddedPixel+targetDir) && !pixelsNotToDraw.Contains(lastAddedPixel+targetDir))
            {
                direction = targetDir;
            }
        }
    }

    private Vector2 Rotate90(Vector2 dir)
    {
        return Quaternion.Euler(0, 0, 90) * dir;
    }

    private bool IsPixelInTexture(Vector2 coord,Vector2 textSize)
    {
        return coord.x > 0 && coord.x < textSize.x && coord.y > 0 && coord.y < textSize.y;
    }
}
