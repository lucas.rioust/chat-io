﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public abstract class Interactable : MonoBehaviour
{
    UnityAction interactAction;

    void Start()
    {
        interactAction = () => Interact();
    }

    void Update()
    {
        
    }

    protected abstract void Interact();

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            PlayerController playerController; 
            if(TryGetComponent<PlayerController>(out playerController))
            {
                if(playerController.player.isMine())
                {
                    playerController.OnInteract.AddListener(interactAction);
                }
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            PlayerController playerController;
            if (TryGetComponent<PlayerController>(out playerController))
            {
                if (playerController.player.isMine())
                {
                    playerController.OnInteract.RemoveListener(interactAction);
                }
            }
        }
    }
}
