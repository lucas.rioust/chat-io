﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Chat : MonoBehaviour
{
    private List<MessageUI> messages = new List<MessageUI>();

    [SerializeField]
    private Transform chatTransform;

    [SerializeField]
    private Scrollbar scrollBar;

    [SerializeField]
    private TMP_InputField inputField;

    [SerializeField]
    private MessageUI messageUIPrefab;

    int increment = 0;

    private bool shouldScrollBarDown = true;

    private void Start()
    {
        scrollBar.onValueChanged.AddListener((f) => OnScrollBarMove(f));
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.Return) && !Input.GetKey(KeyCode.LeftShift))
        {
            SendTextMessage();
        }
    }

    public void AddMessage(Message m)
    {
        var mUI = Instantiate(messageUIPrefab, chatTransform);
        mUI.SetMessage(m);
        mUI.name = m.content;
        
        if(shouldScrollBarDown)
        {
            StartCoroutine(ScrollAfterFrame(5));
        }
    }

    public void Connection(string name)
    {
        var mUI = Instantiate(messageUIPrefab, chatTransform);
        mUI.SetConnect(name);
        mUI.name = name;

        if (shouldScrollBarDown)
        {
            StartCoroutine(ScrollAfterFrame(5));
        }
    }

    public void Disconnection(string name)
    {
        var mUI = Instantiate(messageUIPrefab, chatTransform);
        mUI.SetDisconnect(name);
        mUI.name = name;

        if (shouldScrollBarDown)
        {
            StartCoroutine(ScrollAfterFrame(5));
        }
    }

    public void OnScrollBarMove(float value)
    {
        shouldScrollBarDown = value < .05f;
    }


    IEnumerator ScrollAfterFrame(int nbFrame)
    {
        for (int i = 0; i < nbFrame; i++) yield return null;
        scrollBar.value = 0f;
    }

    public void SendTextMessage()
    {
        if(inputField.text != "")
        {
            NetworkClient.instance.NewBroadcastMessage(inputField.text);
            /*var m = new Message();
            m.author = new Player();
            m.author.username = "801";
            m.author.id = "801";
            m.content = inputField.text;
            AddMessage(m);*/
            inputField.text = "";
        }
    }
}
