﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Player
{
    public string username;
    public string id;

    public bool isMine()
    {
        return username == StaticVariables.PlayerName;
    }
}
