﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MessageUI : MonoBehaviour
{
    [SerializeField]
    protected TextMeshProUGUI text;

    [SerializeField]
    protected Message message;

    [SerializeField]
    private float padding = 20;

    [SerializeField]
    protected RectTransform panel;

    public virtual void SetMessage(Message m)
    {
        string textString = "[" + DateTime.Now.ToShortTimeString() + "] " + "<color=" + "blue" + ">" + m.author.username.ToString() + "</color> : " + m.content;
        text.text = textString;
        message = m;
        StartCoroutine(ResetFrame());
    }

    public void SetDisconnect(string name)
    {
        string textString = "[" + DateTime.Now.ToShortTimeString() + "] " + "<color=" + "red" + ">" + name + " s'est deco comme un batard!</color>";
        text.text = textString;
        StartCoroutine(ResetFrame());
    }

    public void SetConnect(string name)
    {
        string textString = "[" + DateTime.Now.ToShortTimeString() + "] " + "<color=" + "green" + ">" + name + " est arrivé !</color>";
        text.text = textString;
        StartCoroutine(ResetFrame());
    }

    protected IEnumerator ResetFrame()
    {
        yield return null;
        panel.sizeDelta = new Vector2(panel.sizeDelta.x, text.rectTransform.sizeDelta.y + padding); 
    }
}
