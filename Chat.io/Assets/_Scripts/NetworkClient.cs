﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnitySocketIO;
using UnityEditor;

[Serializable]
public class SocketMessage
{
    public string message;
    public SocketMessage(string message)
    {
        this.message = message;
    }

}

[Serializable]
public class SocketId
{
    public string id;
    public SocketId(string id)
    {
        this.id = id;
    }
}

[Serializable]
public class SocketTransform
{
    public SocketTransform(Vector3 pos, Vector3 rot, string id)
    {
        this.pos = pos;
        this.rot = rot;
        this.id = id;
    }
    public Vector3 pos;
    public Vector3 rot;
    public string id;
}
public class NetworkClient : MonoBehaviour
{
    public SocketIOController io;
    [Header("Network Client")]

    public Transform playerSpawner;

    public GameObject playerPrefab;

    private Chat chat;

    public static NetworkClient instance;
    
    private Dictionary<string, GameObject> players;

    public enum State{Launcher, Chat, Room};

    public State state;

    public void Awake()
    {
        if (instance != null)
        {
            Destroy(this);
        }
        instance = this;
        DontDestroyOnLoad(this.gameObject);
    }
    public void Start()
    {
        players = new Dictionary<string, GameObject>();
        SetupEvents();
    }

    public void Connect()
    {
        io.Connect();
    }
    
    public void ChangeScene (State state)
    {
        this.state = state;
    }
    private void SetupEvents()
    {
        io.On("register", (E) =>
        {
            string data = E.data;
            SocketId socketId = JsonUtility.FromJson<SocketId>(data);
            StaticVariables.PlayerId = socketId.id;
            string jsonMessage = JsonUtility.ToJson(new SocketMessage(StaticVariables.PlayerName));
            io.Emit("playerSpawned", jsonMessage);
        });
        
        io.On("spawn", (E) =>
        {
            if (state != State.Chat)
                return;
            string data = E.data;
            Player player = JsonUtility.FromJson<Player>(data);
            GameObject go = new GameObject("ID :" +  player.id );
            if (chat == null)
            {
                chat = GameObject.Find("Chat").GetComponent<Chat>();
            }
            chat.Connection(player.username);
        });

        io.On("chat message", (E) =>
        {
            if (state != State.Chat)
                return;
            string data = E.data;
            Message m = JsonUtility.FromJson<Message>(E.data);
            if (chat == null)
            {
                chat = GameObject.Find("Chat").GetComponent<Chat>();
            }
            chat.AddMessage(m);
        });

        io.On("disconnected", (E) =>
        {
            if (state == State.Chat)
            {

                string data = E.data;
                Player player = JsonUtility.FromJson<Player>(data);
                if (chat == null)
                {
                    chat = GameObject.Find("Chat").GetComponent<Chat>();
                }
                chat.Disconnection(player.username);
            }
            else if (state == State.Room)
            {
                string data = E.data;
                SocketId socketId = JsonUtility.FromJson<SocketId>(data);
                Destroy(players[socketId.id]);
                players.Remove(socketId.id);
            }
        });

        io.On("playerSpawned", (E) =>
        {
            if (state != State.Room)
                return;
            string data = E.data;
            SocketTransform t = JsonUtility.FromJson<SocketTransform>(data);
            players[t.id] = Instantiate(playerPrefab);
            players[t.id].name = "Player_" + t.id;
            if (t.id != StaticVariables.PlayerId)
            {
                players[t.id].GetComponent<PlayerController>().Remove();
            }
            players[t.id].transform.parent = playerSpawner;
            players[t.id].transform.position = t.pos;
            players[t.id].transform.position = t.rot;
            players[t.id].SetActive(true);
        });

        io.On("playerMoved", (E) =>
        {
            if (state != State.Room)
                return;
            string data = E.data;

            SocketTransform t = JsonUtility.FromJson<SocketTransform>(data);
            if (players.ContainsKey(t.id)){
                players[t.id].transform.position = t.pos;
                players[t.id].transform.rotation = Quaternion.Euler(t.rot);
            }
        });

        io.On("playerLeft", (E) =>
        {
            if (state != State.Room)
                return;
            string data = E.data;
            SocketId socketId = JsonUtility.FromJson<SocketId>(data);
            Destroy(players[socketId.id]);
            players.Remove(socketId.id);
        });
       
    }

    public void NewBroadcastMessage(string content)
    {
        string jsonMessage = JsonUtility.ToJson(new SocketMessage(content));
        io.Emit("chat message", jsonMessage);
    }

    public void SpawnPlayer()
    {
        io.Emit("playerSpawned");
    }
    public void NewMovement(SocketTransform t)
    {
        string jsonMessage = JsonUtility.ToJson(t);
        io.Emit("playerMoved", jsonMessage);
    }
}
