﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Launcher : MonoBehaviour
{
    [SerializeField] InputField inputField;
    private void Update()
    {
        if (Input.GetKey(KeyCode.Return) && !Input.GetKey(KeyCode.LeftShift))
        {
            if (inputField.text != "")
            {
                StaticVariables.PlayerName = inputField.text;
                StaticVariables.AlreadyConnected = false;
                NetworkClient.instance.Connect();
                NetworkClient.instance.ChangeScene(NetworkClient.State.Room);
                SceneManager.LoadScene(1);
            }
        }
    }
}
