﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerController : MonoBehaviour
{
    [SerializeField]
    private Rigidbody rigi;

    [SerializeField]
    private Animator animator;

    [SerializeField]
    private float speed;

    [SerializeField]
    private Transform spriteContainer;

    public Player player;

    public UnityEvent OnInteract;

    public PlayerTransformTracking tracking;

    public GameObject camera;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void Remove()
    {
        Destroy(tracking);
        Destroy(camera);
        Destroy(this);
    }
    private void Update()
    {
        Vector3 movementVector = new Vector3(Input.GetAxisRaw("Horizontal"), rigi.velocity.y, Input.GetAxisRaw("Vertical"));
        if(IsThereFloorInFront(movementVector))
        {
            rigi.velocity = Vector3.Lerp(rigi.velocity, movementVector.normalized * speed, .1f);
        }
        else rigi.velocity = Vector3.Lerp(rigi.velocity, Vector3.zero + rigi.velocity.y * Vector3.up, .1f);
        if(rigi.velocity.magnitude > .01f)
        spriteContainer.forward = rigi.velocity;
        animator.SetFloat("Speed", rigi.velocity.magnitude);

        if (Input.GetKeyDown(KeyCode.F))
            OnInteract.Invoke();
    }

    private bool IsThereFloorInFront(Vector3 movementVector)
    {
        Vector3 startPoint = transform.position + .1f * Vector3.up + movementVector.normalized * .3f;
        return Physics.Raycast(startPoint, Vector3.down, 1f);
    }
}
