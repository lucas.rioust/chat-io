﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTransformTracking : MonoBehaviour
{
    private void LateUpdate()
    {
        NetworkClient.instance.NewMovement(new SocketTransform(transform.position, transform.rotation.eulerAngles, StaticVariables.PlayerId));
    }
}
