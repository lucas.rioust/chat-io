var io = require('socket.io')(process.env.PORT || 52300);

//#region  Custom Classes
var Player = require('./Player.js');
var Message = require('./Message.js');
var Transform = require('./Transform.js');
const Vector3 = require('./Transform.js');
//#endregion


// MAIN FUNCTION HERE //

console.log("Server started");

var players = [];
var sockets = [];
var roomPlayers = [];
io.on('connection', (socket) => {

  var player = new Player("defaultPlayer");
  console.log("Connection made");
  
  players[player.id] = player;
  sockets[player.id] = socket;
  socket.emit('register', {id: player.id});  //Tell the client its ID for the server.

  socket.on('pseudo', (msg) =>{
    player.username = msg.message;
    console.log(msg.message);
    socket.emit('spawn', player); //Tell the client he spawned.
    socket.broadcast.emit('spawn', player); //Tell the other clients someone spawned.

    for(var playerID in players) //Inform the client of all the other players connected.
    {
      if (playerID != player.id){
        socket.emit('spawn', players[playerID]);
      }
    }
  });

  socket.on('disconnect', () =>{
    console.log("Client\ " + player.id + "\ disconnected");
    delete players[player.id];
    delete sockets[player.id];
    socket.broadcast.emit('disconnected', player);
  })

  socket.on('chat message', (msg) => {
    var message = new Message(msg.message, player);
    console.log(player.id + "\ sent chat message :\ " + message.content);
    socket.emit('chat message', message);
    socket.broadcast.emit('chat message', message);
  });

  socket.on('playerSpawned', () => {
    roomPlayers[player.id] = new Transform(new Vector3(0.0,0.0,0.0), new Vector3(0.0,0.0,0.0), player.id);
    socket.broadcast.emit('playerSpawned', roomPlayers[player.id]);
    socket.emit('playerSpawned', roomPlayers[player.id]);
    for(var playerID in roomPlayers) //Inform the client of all the other players connected.
    {
      if (playerID != player.id){
        socket.emit('playerSpawned', roomPlayers[playerID]);
      }
    }
  });

  socket.on('playerMoved', (msg) => {
    roomPlayers[player.id] = new Transform(msg.pos, msg.rot, player.id);
    socket.broadcast.emit('playerMoved', roomPlayers[player.id]);
  })

  socket.on('playerLeft', () =>{
    socket.broadcast.emit('playerLeft', player.id);
  });
});