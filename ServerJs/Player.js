var shortID = require('shortid');

module.exports = class Player {
    constructor(pseudo) {
        this.username = pseudo;
        this.id = shortID.generate();
    }
}