module.exports = class Message {
    constructor(content, player) {
        this.content = content;
        this.author = player;
    }
}